// importing mangoose from mangoose
import mongoose from "mongoose";

/** 
    creating a schema for register_data
    in that withrespected datatypes
*/
const REGISTER_SCHEMA = new mongoose.Schema({
  name: String,
  username: String,
  password: String,
  email: String,
  phone_number: Number,
  security_question: String,
});

/**
    creating a schema for the login_data
    withrespected datatypes
*/
const LOGIN_SCHEMA = new mongoose.Schema({
  username: String,
  password: String,
  jwt_token: String,
  logon_time: Date,
});
/*
   creating a schema for product section with respected datatypes 

*/


/**
    creating a schema for the post_data
    withrespected datatypes
*/
const POST_SCHEMA = new mongoose.Schema({
  title: String,
  image: String,
  type: String,
  price: String,
  rating: String
});

/** 
   * creating a schema for the detailed_product_data
   * withrespected datatypes
*/
const DETAILED_PRODUCT = new mongoose.Schema({
  product_id: String,
  title: String,
  price: String,
  rating: String,
  reviews :Number,
  description: String,
  availability: String,
  type: String,
  image: String,
  similar_products: Array
})

/** 
   * creating a schema for the detailed_product_data
   * withrespected datatypes
*/
const CAROUSEL_IMAGES = new mongoose.Schema({
  
  image: String,
  
})


/*exporting login_schema,register_schema,POST_SCHEMA,
  DETAILED_PRODUCT*/

export {
  LOGIN_SCHEMA,
  REGISTER_SCHEMA,
  POST_SCHEMA,
  DETAILED_PRODUCT,
  CAROUSEL_IMAGES
}
