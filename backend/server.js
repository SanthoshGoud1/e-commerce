//external dependencies
import express from "express";
import bodyParser from "body-parser";
//initialzing app variable to express
const app = express();
//Returns middleware that only parses json
app.use(bodyParser.json());
//Returns middleware that only parses urlencoded bodies
app.use(bodyParser.urlencoded({ extended: true }));
//CORS enables you to access a resource from a different origin
import cors from "cors";
app.use(
  cors({
    origin: "*",
  })
);

//internal depedencies
// importing get api's handlers from get api's modules
import {
  get_the_postdata,
  get_the_red_meat,
  get_the_poultry,
  get_the_pork,
  get_the_seefood,
  get_the_prime_products,
  get_the_detailed_products,
  get_the_carousel_images,
} from "./path_handlers/get_apis.js";

import { deleting_login_data } from "./path_handlers/delete_apis.js";

// importing the login_user_handler and register_user_handler from the postapis file
import{
    login_user_handler,
    register_user_handler,
    post_the_data,
    post_the_prime_details,

   post_carousel_images,
    post_the_detailed_product
   
    }
     from './path_handlers/post_apis.js'

// importing the forgot_user_handler from the putapis file
import { forgot_user_handler } from "./path_handlers/put_apis.js";

/****API CALLS */ 

/****************************POST************************************** */  
// getiing the user data from client and store it into database
app.post("/register/:user_type", register_user_handler);

// validating the user data and given authentication
app.post("/login/:user_type", login_user_handler);

// post the prime details
app.post("/primedetails", post_the_prime_details);

// sending the porducts
app.post("/posts", post_the_data);

// post the prime details
app.post("/primedetails", post_the_prime_details);

// post the detailed product
app.post("/detailed/products", post_the_detailed_product);

// post the carousel images 
app.post("/carousel/images", post_carousel_images);

app.delete("/logout", deleting_login_data);

// get prime products
app.get("/prime/products", get_the_prime_products)

// post the detailed product
app.post("/detailed/products", post_the_detailed_product)


/****************************DELETE************************************** */ 
app.delete("/logout", deleting_login_data);


/****************************GET************************************** */ 
// get prime products
app.get("/prime/products", get_the_prime_products)

// get detailed products
app.get("/detailed_product", get_the_detailed_products)

// get detailed products
app.get("/detailed_product/:id", get_the_detailed_products);

// getting all products
app.get("/posts/data", get_the_postdata);

// getting the all redmeatproducts
app.get("/products/red_meat", get_the_red_meat);

// getting the all poultry products
app.get("/products/poultry", get_the_poultry);

// getting the all pork products
app.get("/products/pork", get_the_pork);

// getting the all carousel images
app.get("/carousel/images", get_the_carousel_images);


// getting the all seafood products
app.get("/products/seafood", get_the_seefood);


/****************************PUT************************************** */ 
// updating the user password
app.put("/forgot/:user_type", forgot_user_handler);


// this server is listening the 3000 port
app.listen(3001, () => {
  console.log("http://localhost:3001");
});
