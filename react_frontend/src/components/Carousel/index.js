//imported external dependencies
import {Component} from 'react'
import Cookies from 'js-cookie'
import Slider from 'react-slick'
// import Loader from 'react-loader-spinner'
import './index.css'

//class component for carousel 
class Carousel extends Component {
  state = {
    carouselList: [],
    isLoading: false,
  }

  componentDidMount() {
    this.getCarousel()
  }

  getCarousel = async () => {
    this.setState({
      isLoading: true,
    })
    const jwtToken = Cookies.get('jwt_token')
    const url = 'http://localhost:3001/carousel/images'
    const options = {
      // headers: {
      //   Authorization: `Bearer ${jwtToken}`,
      // },
      method: 'GET',
      headers: {
        "Content-Type":"application/json",
        "Accept":"application/json"
      }
    }
    const response = await fetch(url, options)
    console.log(response)
    const data = await response.json()
    console.log(data)
    const updatedData = data.map(eachItem => ({
      id: eachItem.id,
      imageUrl: eachItem.image,
    }))
    this.setState({
      carouselList: updatedData,
      isLoading: false,
    })
  }

  renderCarouselList = () => {
    const settings = {
      dots: true,
      infinite: true,
      speed: 1000,
      slidesToShow: 1,
      slidesToScroll: 1,
      autoplay: true,
      autoplaySpeed: 2000,
      pauseOnHover: true,
    }
    const {carouselList} = this.state
    //returns the component of carousel used map method to render the images in the component 
    return (
      <ul className="bg-container">
        <Slider {...settings}>
          {carouselList.map(eachImage => (
            <li key={eachImage.id}>
              <img src={eachImage.imageUrl} alt="offer" className="image" />
            </li>
          ))}
        </Slider>
      </ul>
    )
  }

  renderLoader = () => (
    <div testid="restaurants-offers-loader" className="carousel-loader">
      {/* <Loader type="ThreeDots" color="#F7931E" height={50} width={50} /> */}
    </div>
  )

  render() {
    return  this.renderCarouselList()
  }
}

//exporting carousel module
export default Carousel
