import {Link} from 'react-router-dom'
//imported CSS file for styling
import './index.css'


{/*This function is for if cart is empty, 
  used LINK to render from click shop now button so user will redirect to product details
*/}

const EmptyCartView = () => (
  <div className="cart-empty-view-container">
    <img
      src="http://localhost:3000/images/allproductssection/empty-cart.png"
      className="cart-empty-image"
      alt="cart empty"
    />
    <h1 className="cart-empty-heading">Your Cart Is Empty</h1>

    <Link to="/products">
      <button type="button" className="shop-now-btn">
        Shop Now
      </button>
    </Link>
  </div>
)

//exporting emptycartview module
export default EmptyCartView
