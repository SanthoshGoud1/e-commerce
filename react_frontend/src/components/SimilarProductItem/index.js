//imported external dependencies
import { Link } from "react-router-dom";
//imported CSS for styling
import "./index.css";

//written a function for gathering the similar products by sending props.
//passed some keys as props for getting the data
const SimilarProductItem = (props) => {
  const { productDetails } = props;
  const { title, brand, imageUrl, rating, price, id } = productDetails;
//returning the products in the User Interface
  return (
      <li className="similar-product-item">
        <img
          src={imageUrl}
          className="similar-product-image"
          alt={`similar product ${title}`}
        />
        <p className="similar-product-title">{title}</p>
        <p className="similar-products-brand">by {brand}</p>
        <div className="similar-product-price-rating-container">
          <p className="similar-product-price">Rs {price}/-</p>
          <div className="similar-product-rating-container">
            <p className="similar-product-rating">{rating}</p>
            <img
              src="http://localhost:3000/images/productcard/star.png"
              alt="star"
              className="similar-product-star"
            />
          </div>
        </div>
      </li>
  );
};

//exported the function for further actions 
export default SimilarProductItem;
