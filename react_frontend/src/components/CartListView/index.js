//imported internal dependencies
import CartItem from "../CartItem";
import CartContext from "../../context/CartContext";
//imported CSS file for styling
import "./index.css";

{/*To keep context re-rendering fast, React needs to make each context consumer a separate node in the tree.
  *To view all cart lists using map to iterate over an array and manipulate or change data items 
*/}

const CartListView = () => (
  <CartContext.Consumer>
    {(value) => {
      const { cartList,totalPrice } = value;
      console.log(totalPrice)
      return (
        <ul className="cart-list">
          {cartList.map((eachCartItem) => (
            <CartItem key={eachCartItem.id} cartItemDetails={eachCartItem} />
          ))}
        </ul>
      );
    }}
  </CartContext.Consumer>
);

//exporting cartlistview module
export default CartListView;
