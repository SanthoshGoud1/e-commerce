//imported external dependencies
import { Link } from "react-router-dom";
//imported css file for styling
import "./index.css";


//using props  to pass data from one component to another
const ProductCard = props => {
  const {productData} = props
  const {title, type, imageUrl, rating, price, id} = productData
//to view the product details
  return (
    <Link to={`/products/${id}`} className="link-item">
      <li className="product-item">
        <img src={imageUrl} alt="product" className="thumbnail" />
        <h1 className="title">{title}</h1>
        <p className="brand">by {type}</p>
        <div className="product-details">
          <p className="price">{price}</p>
          <div className="rating-container">
            <p className="rating">{rating}</p>
            <img
              src="http://localhost:3000/images/productcard/star.png"
              alt="star"
              className="star"
            />
          </div>
        </div>
      </li>
    </Link>
  );
};

//exporting productcard module
export default ProductCard;
